class Square:
	"""docstring for Square"""
	def __init__(self, x,y, color, state ):
		self.x = x
		self.y = y
		self.color = color
		self.state = state
	
	def __repr__(self):
		return 'Square(x=%s, y=%s,color%s,state=%s)' % (self.x, self.y, self.color,self.state)

	def displayEmployee(self):
		print "X : ", self.x, ", Y: ", self.y,"Color : ", self.color