#Snake Game

import pygame , sys , random , time , copy , numpy , operator , csv
from Square import Square
from pprint import pprint

check_errors = pygame.init()

if check_errors[1] > 0 :
	print("(!) Had {0} initializing errors, exiting....".format(check_errors[1]))
	sys.exit(-1)
else:
	print("PyGame Successfully initialized")

Square1 = Square(1,1,"Red","Empty")

# Ahmed = Square1.displayEmployee()
#Play Surface

playSurface = pygame.display.set_mode((1200,600))
#pygame.display.set_caption(" Dodo")

file = csv.writer(open('test.csv','w'))
file.writerow(['UsersOffer','AgentOffer','AgentAccepted'
	, 'UserAccepted','Start Position User','Start Position Agent'
	, 'Beginning ChipSet User','Beginning ChipSet Agent'
	, 'Beginning Status User','Beginning Status Agent'
	, "PostOfferStatusUser", "PostOfferStatusAgent"
	, "BeginningPointUser", "BeginningPointAgent"
	, "PostPointUser", "PostPointAgent"
	, "postResponseStatusUser","postResponseStatusAgent"
	, "postResponsePointsUser","postResponsePointsAgent"
	, "restOfChipsUser" , "Elapsed Time" , "Clicks User" , "Clicks Agent"
	,"Number of Offered Chips User" , "Number of Offered Chips Agent"
	])

#Colors
red = pygame.Color(255,0,0) 	   #gameover
blue = pygame.Color(0,0,255) 	   #snake
green = pygame.Color(3,100,3) 
violet = pygame.Color(70,5,145) 	   #snake
black = pygame.Color(0,0,0) 	   #score
white = pygame.Color(255,255,255)  #background
yellow = pygame.Color(255,250,42)   #food
grey = pygame.Color(119,119,119)   #food

userChipSet = [['Red',1],['Yellow',2],['Blue',3],['Green',5],['Violet',3]]
agentChipSet = [['Red',1],['Yellow',2],['Blue',3],['Green',5],['Violet',3]]
global offeredChipSetUser 
global offeredChipSetAgent
global offeredChipSetUserCopy 
global offeredChipSetAgentCopy
global remainingChipSetUser
global remainingChipSetAgent
global ChipSetUserBeginning
global ChipSetAgentBeginning
global BeginningStatusUser
global BeginningStatusAgent
global PostOfferStatusUser
global PostOfferStatusAgent
global BeginningPointUser
global BeginningPointAgent
global PostPointUser
global PostPointAgent
global restOfChipsUser
global restOfChipsAgent

offeredChipSetAgentCopy=0
offeredChipSetUserCopy=0
restOfChipsUser=0
restOfChipsAgent=0
testRemainingUser=0
testOfferedUser=0
testRemainingAgent=0
testOfferedAgent=0

testRemainingUser=0
testOfferedUser=0
testRemainingAgent=0
testOfferedAgent=0


postResponseStatusUser = 0
postResponseStatusAgent = 0
postResponsePointsUser = 0
postResponsePointsAgent = 0

userPosXTestStart=0
userPosYTestStart=0
userPosXTestPost=0
userPosYTestPost=0
agentPosXTestStart=0
agentPosYTestStart=0
agentPosXTestPost=0
agentPosYTestPost=0
restOfChipsUser=[]
restOfChipsAgent=[]

offeredChipSetUser = [0,0,0,0,0]
offeredChipSetAgent = [0,0,0,0,0]
remainingChipSetUser =[]
remainingChipSetAgent =[]


global route 
route=[]
firstTime = True
userTurn = 1

agentAccepted = 3
userAccepted = 3


beginningFlag = True
global userPosX
global userPosY
global agentPosX
global agentPosY
global userPosXCSV
global userPosYCSV
global agentPosXCSV
global agentPosYCSV
global rejectXUser
global rejectYUser
global rejectXAgent
global rejectYAgent
global acceptXUser
global acceptYUser
global acceptXAgent
global acceptYAgent
global userPosYCSV
global agentPosXCSV
global agentPosYCSV
userPosX = 0
userPosY = 0
agentPosX = 0
agentPosY = 0

global clicksUser
global clicksAgent
clicksUser = 0 
clicksAgent = 0


global hintText
hintText = ''

global NextRound
NextRound = 1

#FPS controller
fpsController = pygame.time.Clock()


score = 0

global visited
visited = [[-1 for _ in range(4)] for _ in range(4)]


pprint(visited)
Grid = [[Square(0,0,"Red","Empty") for _ in range(4)] for _ in range(4)]


requiredPathUser = [[0,0,0,0,0], #DownDownRightRight
[0,0,0,0,0], #RightRightDownDown
[0,0,0,0,0], #DownRightDownRight
[0,0,0,0,0]] #RightDownRightDown

requiredPathAgent = [[0,0,0,0,0], #DownDownRightRight
[0,0,0,0,0], #RightRightDownDown
[0,0,0,0,0], #DownRightDownRight
[0,0,0,0,0]] #RightDownRightDown


#Flags TCI Options
ChooseCoopertivness=1
ChoosePersistance=1
ChooseRevengfulness=1

PersistanceFlag = 0 

playSurface.fill(white)

def drawTCIOptions():
	myFont = pygame.font.SysFont('monaco', 30)
		#Grey Button
	Rect = pygame.Rect(50,500,200,40)
	pygame.draw.rect(playSurface,black,Rect,5)
	pygame.draw.rect(playSurface,grey,Rect)

	GOsurf = myFont.render('Start Game', True, black)
	Gorect = GOsurf.get_rect()
	Gorect.midtop = (150,510)
	playSurface.blit(GOsurf,Gorect)

	Rect = pygame.Rect(50,400,200,40)
	pygame.draw.rect(playSurface,black,Rect,5)
	pygame.draw.rect(playSurface,grey,Rect)

	GOsurf = myFont.render('Cooperativness', True, black)
	Gorect = GOsurf.get_rect()
	Gorect.midtop = (130,405)
	playSurface.blit(GOsurf,Gorect)


	Rect = pygame.Rect(50,300,200,40)
	pygame.draw.rect(playSurface,black,Rect,5)
	pygame.draw.rect(playSurface,grey,Rect)

	GOsurf = myFont.render('Persistance', True, black)
	Gorect = GOsurf.get_rect()
	Gorect.midtop = (130,305)
	playSurface.blit(GOsurf,Gorect)

	Rect = pygame.Rect(50,200,200,40)
	pygame.draw.rect(playSurface,black,Rect,5)
	pygame.draw.rect(playSurface,grey,Rect)

	GOsurf = myFont.render('Revengfulness', True, black)
	Gorect = GOsurf.get_rect()
	Gorect.midtop = (130,205)
	playSurface.blit(GOsurf,Gorect)

	myFont = pygame.font.SysFont('monaco', 20)

	if ChooseCoopertivness==1:
		GOsurf = myFont.render('Choose', True, black)
	else:
		GOsurf = myFont.render('Chosen', True, black)	
	
	Gorect = GOsurf.get_rect()
	Gorect.midtop = (220,425)
	playSurface.blit(GOsurf,Gorect)
	if ChoosePersistance==1:
		GOsurf = myFont.render('Choose', True, black)
	else:
		GOsurf = myFont.render('Chosen', True, black)	
	
	Gorect = GOsurf.get_rect()
	Gorect.midtop = (220,325)
	playSurface.blit(GOsurf,Gorect)

	if ChooseRevengfulness==1:
		GOsurf = myFont.render('Choose', True, black)
	else:
		GOsurf = myFont.render('Chosen', True, black)	
	
	Gorect = GOsurf.get_rect()
	Gorect.midtop = (220,225)
	playSurface.blit(GOsurf,Gorect)

startGameFlag = True
while True:

	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			pygame.quit()
			sys.exit()
		elif event.type == pygame.KEYDOWN:
			if event.key == pygame.K_ESCAPE:
				pygame.event.post(pygame.event.Event('QUIT'))
		elif event.type == pygame.MOUSEBUTTONDOWN:
			pos = pygame.mouse.get_pos()
			if pos[0]>50 and pos[0]<250:
				if pos[1]<540 and pos[1]>500:
					startGameFlag=False
					break
				elif pos[1]<440 and pos[1]>400:
					ChooseCoopertivness = ~ ChooseCoopertivness

				elif pos[1]<340 and pos[1]>300:
					ChoosePersistance = ~ ChoosePersistance

				elif pos[1]<240 and pos[1]>200:
					ChooseRevengfulness = ~ ChooseRevengfulness

	if startGameFlag != True :
		break
	playSurface.fill(white)
	drawTCIOptions()

	pygame.display.flip()
	fpsController.tick(50)




for roundNumber in range(0,10):
	startTime = time.time()
	PersistanceFlag = 0
	if roundNumber%10 != 7:
		
		for x in range(0, 4):
			for y in range(0,4):
				Random = random.randrange(0,5)
				
				if Random == 1:
					Grid[x][y] = Square(x,y,"Blue","Empty")
				elif Random == 2:
					Grid[x][y] = Square(x,y,"Yellow","Empty")
				elif Random == 3:
					Grid[x][y] = Square(x,y,"Red","Empty")

				elif Random == 4:
					Grid[x][y] = Square(x,y,"Violet","Empty")

				else:
					Grid[x][y] = Square(x,y,"Green","Empty")
	else:
		PersistanceFlag = 1
	Grid[3][3].state='GOAL'

	def updateRequiredPath1User(x,y):
		print x
		print y
		while x<3 : 
			x+=1
			if Grid[x][y].color == 'Blue':
				requiredPathUser[0][1]+=1
			elif Grid[x][y].color == 'Yellow':
				requiredPathUser[0][3]+=1
			elif Grid[x][y].color == 'Red':
				requiredPathUser[0][0]+=1	
			elif Grid[x][y].color == 'Violet':
				requiredPathUser[0][4]+=1
			else:
				requiredPathUser[0][2]+=1

		while y<3:
			y+=1
			if Grid[x][y].color == 'Blue':
				requiredPathUser[0][1]+=1
			elif Grid[x][y].color == 'Yellow':
				requiredPathUser[0][3]+=1
			elif Grid[x][y].color == 'Red':
				requiredPathUser[0][0]+=1	
			elif Grid[x][y].color == 'Violet':
				requiredPathUser[0][4]+=1
			else:
				requiredPathUser[0][2]+=1

	def updateRequiredPath2User(x,y):

		while y<3 : 
			y+=1
			if Grid[x][y].color == 'Blue':
				requiredPathUser[1][1]+=1
			elif Grid[x][y].color == 'Yellow':
				requiredPathUser[1][3]+=1
			elif Grid[x][y].color == 'Red':
				requiredPathUser[1][0]+=1	
			elif Grid[x][y].color == 'Violet':
				requiredPathUser[1][4]+=1
			else:
				requiredPathUser[1][2]+=1

		while x<3:
			x+=1
			if Grid[x][y].color == 'Blue':
				requiredPathUser[1][1]+=1
			elif Grid[x][y].color == 'Yellow':
				requiredPathUser[1][3]+=1
			elif Grid[x][y].color == 'Red':
				requiredPathUser[1][0]+=1	
			elif Grid[x][y].color == 'Violet':
				requiredPathUser[1][4]+=1
			else:
				requiredPathUser[1][2]+=1


	def updateRequiredPath3User(x,y):
		direction = 'RIGHT'
		down = False
		while x<=3 and y<=3:
			if x==3 and y==3:
				break
			if down:
				down = False
				if x<3:
					x+=1

					if Grid[x][y].color == 'Blue':
						requiredPathUser[2][1]+=1
					elif Grid[x][y].color == 'Yellow':
						requiredPathUser[2][3]+=1
					elif Grid[x][y].color == 'Red':
						requiredPathUser[2][0]+=1	
					elif Grid[x][y].color == 'Violet':
						requiredPathUser[2][4]+=1
					else:
						requiredPathUser[2][2]+=1

					
			else:
				down = True
				if y<3:
					y+=1

					if Grid[x][y].color == 'Blue':
						requiredPathUser[2][1]+=1
					elif Grid[x][y].color == 'Yellow':
						requiredPathUser[2][3]+=1
					elif Grid[x][y].color == 'Red':
						requiredPathUser[2][0]+=1	
					elif Grid[x][y].color == 'Violet':
						requiredPathUser[2][4]+=1
					else:
						requiredPathUser[2][2]+=1



	def updateRequiredPath4User(x,y):
		direction = 'RIGHT'
		down = True
		while x<=3 and y<=3:
			if x==3 and y==3:
				break
			if down:
				down = False
				if x<3:
					x+=1

					if Grid[x][y].color == 'Blue':
						requiredPathUser[3][1]+=1
					elif Grid[x][y].color == 'Yellow':
						requiredPathUser[3][3]+=1
					elif Grid[x][y].color == 'Red':
						requiredPathUser[3][0]+=1	
					elif Grid[x][y].color == 'Violet':
						requiredPathUser[3][4]+=1
					else:
						requiredPathUser[3][2]+=1

			else:
				down = True
				if y<3:
					y+=1
					if Grid[x][y].color == 'Blue':
						requiredPathUser[3][1]+=1
					elif Grid[x][y].color == 'Yellow':
						requiredPathUser[3][3]+=1
					elif Grid[x][y].color == 'Red':
						requiredPathUser[3][0]+=1	
					elif Grid[x][y].color == 'Violet':
						requiredPathUser[3][4]+=1
					else:
						requiredPathUser[3][2]+=1



	def updateRequiredPath1Agent(x,y):
		print x
		print y
		while x<3 : 
			x+=1
			if Grid[x][y].color == 'Blue':
				requiredPathAgent[0][1]+=1
			elif Grid[x][y].color == 'Yellow':
				requiredPathAgent[0][3]+=1
			elif Grid[x][y].color == 'Red':
				requiredPathAgent[0][0]+=1	
			elif Grid[x][y].color == 'Violet':
				requiredPathAgent[0][4]+=1
			else:
				requiredPathAgent[0][2]+=1

		while y<3:
			y+=1
			if Grid[x][y].color == 'Blue':
				requiredPathAgent[0][1]+=1
			elif Grid[x][y].color == 'Yellow':
				requiredPathAgent[0][3]+=1
			elif Grid[x][y].color == 'Red':
				requiredPathAgent[0][0]+=1	
			elif Grid[x][y].color == 'Violet':
				requiredPathAgent[0][4]+=1
			else:
				requiredPathAgent[0][2]+=1

	def updateRequiredPath2Agent(x,y):

		while y<3 : 
			y+=1
			if Grid[x][y].color == 'Blue':
				requiredPathAgent[1][1]+=1
			elif Grid[x][y].color == 'Yellow':
				requiredPathAgent[1][3]+=1
			elif Grid[x][y].color == 'Red':
				requiredPathAgent[1][0]+=1	
			elif Grid[x][y].color == 'Violet':
				requiredPathAgent[1][4]+=1
			else:
				requiredPathAgent[1][2]+=1

		while x<3:
			x+=1
			if Grid[x][y].color == 'Blue':
				requiredPathAgent[1][1]+=1
			elif Grid[x][y].color == 'Yellow':
				requiredPathAgent[1][3]+=1
			elif Grid[x][y].color == 'Red':
				requiredPathAgent[1][0]+=1	
			elif Grid[x][y].color == 'Violet':
				requiredPathAgent[1][4]+=1
			else:
				requiredPathAgent[1][2]+=1


	def updateRequiredPath3Agent(x,y):
		direction = 'RIGHT'
		down = False
		while x<=3 and y<=3:
			if x==3 and y==3:
				break
			if down:
				down = False
				if x<3:
					x+=1
					if Grid[x][y].color == 'Blue':
						requiredPathAgent[2][1]+=1
					elif Grid[x][y].color == 'Yellow':
						requiredPathAgent[2][3]+=1
					elif Grid[x][y].color == 'Red':
						requiredPathAgent[2][0]+=1	
					elif Grid[x][y].color == 'Violet':
						requiredPathAgent[2][4]+=1
					else:
						requiredPathAgent[2][2]+=1

			else:
				down = True
				if y<3:
					y+=1

					if Grid[x][y].color == 'Blue':
						requiredPathAgent[2][1]+=1
					elif Grid[x][y].color == 'Yellow':
						requiredPathAgent[2][3]+=1
					elif Grid[x][y].color == 'Red':
						requiredPathAgent[2][0]+=1	
					elif Grid[x][y].color == 'Violet':
						requiredPathAgent[2][4]+=1
					else:
						requiredPathAgent[2][2]+=1


	def updateRequiredPath4Agent(x,y):
		direction = 'RIGHT'
		down = True
		while x<=3 and y<=3:
			if x==3 and y==3:
				break
			if down:
				down = False
				if x<3:
					x+=1

					if Grid[x][y].color == 'Blue':
						requiredPathAgent[3][1]+=1
					elif Grid[x][y].color == 'Yellow':
						requiredPathAgent[3][3]+=1
					elif Grid[x][y].color == 'Red':
						requiredPathAgent[3][0]+=1	
					elif Grid[x][y].color == 'Violet':
						requiredPathAgent[3][4]+=1
					else:
						requiredPathAgent[3][2]+=1
					
			else:
				down = True
				if y<3:
					y+=1

					if Grid[x][y].color == 'Blue':
						requiredPathAgent[3][1]+=1
					elif Grid[x][y].color == 'Yellow':
						requiredPathAgent[3][3]+=1
					elif Grid[x][y].color == 'Red':
						requiredPathAgent[3][0]+=1	
					elif Grid[x][y].color == 'Violet':
						requiredPathAgent[3][4]+=1
					else:
						requiredPathAgent[3][2]+=1


	def drawButtons():
		#Grey Button
		Rect = pygame.Rect(50,500,200,40)
		pygame.draw.rect(playSurface,black,Rect,5)
		pygame.draw.rect(playSurface,grey,Rect)

		#Offer Text
		myFont = pygame.font.SysFont('monaco', 35)
		if userTurn==1:
			GOsurf = myFont.render('Send Offer', True, black)
		elif userTurn==2:
			GOsurf = myFont.render('Receive Offer', True, black)
		else:
			GOsurf = myFont.render('Next Round', True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (150,510)
		playSurface.blit(GOsurf,Gorect)

	def showPoints():
		#Grey Button
		myFont = pygame.font.SysFont('monaco', 25)
		GOsurf = myFont.render("User's Points: {0}".format(postResponsePointsUser), True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (350,430)
		playSurface.blit(GOsurf,Gorect)
		GOsurf = myFont.render("Agent's Points: {0}".format(postResponsePointsAgent), True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (350,455)
		playSurface.blit(GOsurf,Gorect)


	def updateChipSet(color , ChipSet):
		if color=='Red':
			ChipSet[0]-=1
				
		elif color=='Blue':
				
			ChipSet[1]-=1	
				
		elif color=='Green':
			
			ChipSet[2]-=1	
				
		elif color=='Yellow':
				
			ChipSet[3]-=1	

		else:
			
			ChipSet[4]-=1

	def getChipSetAfterRoute(x, y, ChipSet):

		for element in route:
			
			if element=='Down':
				y=y+1
			elif element=='Right':
				x=x+1
			elif element=='Top':
				y=y-1
			else:
				x=x-1

			updateChipSet(Grid[x][y].color,ChipSet)
		return ChipSet	
			


	def allocateChips(ChipSet):
		agent = True
		pprint("ChipSet")
		pprint(ChipSet)
		global route
		for x in range(0,20):
			test = copy.copy(ChipSet)
			agentChips=[0,0,0,0,0]
			userChips=[0,0,0,0,0]
			while test[0]!=0 or test[1]!=0 or test[2]!=0 or test[3]!=0 or test[4]!=0:
				index = random.randrange(0,5)
				if test[index]>0:
					test[index]-=1
					if agent:
						agentChips[index]+=1
						agent = False
					else:
						userChips[index]+=1
						agent = True
			if analyzeChipSet(agentPosX,agentPosY,0,agentChips):
				pprint("agentChips")
				print route
				pprint(agentChips)
				route=[]
				if analyzeChipSet(userPosX,userPosY,0,userChips) :
					pprint("userChips")
					print route
					pprint(userChips)
					return [agentChips,userChips]
					break

		return []
					


	def makeAnOffer(x,y,AgentChipSet,UserChipSet):
		newChipSet=[AgentChipSet[0]+UserChipSet[0]
		,AgentChipSet[1]+UserChipSet[1],
		AgentChipSet[2]+UserChipSet[2],
		AgentChipSet[3]+UserChipSet[3],
		AgentChipSet[4]+UserChipSet[4]]

		# pprint(newChipSet)
		allocation = allocateChips(newChipSet)
		if len(allocation)!=0:
			newAgentChipSet = allocation[0]
			newUserChipSet = allocation[1]
		else:
			analyzeChipSet(x,y,0,newChipSet)
			pprint(route)
			beforeRoute = copy.copy(newChipSet)
			newChipSet = getChipSetAfterRoute(x,y,newChipSet)
			newAgentChipSet = numpy.subtract(beforeRoute,newChipSet)
		global offeredChipSetAgent 
		global offeredChipSetUser 
		global remainingChipSetAgent 
		global remainingChipSetUser 
		
		agentOrUser = random.randrange(0,10)
		if agentOrUser > 2 :
			if len(allocation)!=0:
				newAgentChipSet = allocation[0]
			
			offeredChipSetAgent = map(operator.sub, newAgentChipSet, AgentChipSet)
			
			for x in range(0, 5):
				if offeredChipSetAgent[x]>=0:
					offeredChipSetUser[x] = offeredChipSetAgent[x]
					offeredChipSetAgent[x] = 0
				else:
					offeredChipSetAgent[x] = abs(offeredChipSetAgent[x])
			pprint("Selfish Agent")
			remainingChipSetAgent = map(operator.sub, AgentChipSet,offeredChipSetAgent)
			remainingChipSetUser = map(operator.sub, UserChipSet,offeredChipSetUser)
		else:
			if len(allocation)!=0:
				newAgentChipSet = allocation[1]
			offeredChipSetUser = map(operator.sub, newAgentChipSet, UserChipSet)
			
			for x in range(0, 5):
				if offeredChipSetUser[x]>=0:
					offeredChipSetAgent[x] = offeredChipSetUser[x]
					offeredChipSetUser[x] = 0
				else:
					offeredChipSetUser[x] = abs(offeredChipSetUser[x])
			pprint("Help User")
			remainingChipSetUser = map(operator.sub, UserChipSet,offeredChipSetUser)
			remainingChipSetAgent = map(operator.sub, AgentChipSet,offeredChipSetAgent)





	def analyzeOffer(x,y,remainingChipSet,OfferedChipSet):
		newChipSet=[remainingChipSet[0]+OfferedChipSet[0],remainingChipSet[1]+OfferedChipSet[1],remainingChipSet[2]+OfferedChipSet[2],
		remainingChipSet[3]+OfferedChipSet[3],remainingChipSet[4]+OfferedChipSet[4]]

		return analyzeChipSet(x,y,0,newChipSet)



	def analyzeChipSet(x, y, previous, ChipSet,default=1):

		global agentAccepted
		global restOfChipsUser
		global restOfChipsAgent
		global route
		if x < 0 or x > 3 or y < 0 or y > 3:
			return 0
		
		
		# if visited[x][y] != -1:
		# 	print 'HERE'
		# 	return visited[x][y]



		if ChipSet[0]==0 and ChipSet[1]==0 and ChipSet[2]==0 and ChipSet[3]==0 and ChipSet[4]==0:
			return 0


		if previous==1:
			if Grid[x][y].color=='Red':
				if ChipSet[0]>0:
					ChipSet[0]-=1
				else:
					return 0
			elif Grid[x][y].color=='Blue':
				if ChipSet[1]>0:
					ChipSet[1]-=1	
				else:
					return 0
			elif Grid[x][y].color=='Green':
				if ChipSet[2]>0:
					ChipSet[2]-=1	
				else:
					return 0 
			elif Grid[x][y].color=='Yellow':
				if ChipSet[3]>0:
					ChipSet[3]-=1	
				else:
					return 0
			else:
				if ChipSet[4]>0:
					ChipSet[4]-=1	
				else:
					return 0

			if x==3 and y==3:
				return 1
		else:
			previous=1

		rightChipSet = copy.copy(ChipSet)
		topChipSet = copy.copy(ChipSet)
		leftChipSet = copy.copy(ChipSet)
		bottomChipSet = copy.copy(ChipSet)

		bottomRoute = analyzeChipSet(x,y+1,previous,bottomChipSet,default)
		if bottomRoute == 1:
			visited[x][y]=1
			route.insert(0,'Down')
			agentAccepted=1
			return 1
		rightRoute = analyzeChipSet(x+1,y,previous,rightChipSet,default) 
		if rightRoute == 1: 
			visited[x][y]=1
			route.insert(0,'Right')
			agentAccepted=1
			return 1

		topRoute =	analyzeChipSet(x,y-1,previous,topChipSet,default)
		if topRoute == 1:
			visited[x][y]=1
			route.insert(0,'Top')
			agentAccepted=1 
			return 1
		leftRoute = analyzeChipSet(x-1,y,previous,leftChipSet,default)
		if leftRoute == 1:
			visited[x][y]=1
			route.insert(0,'Left')
			agentAccepted=1
			return 1
		
		else:
			agentAccepted=0
			return 0 

	def maxPoints(x, y, previous, ChipSet,default=1):

		global userPosY
		global userPosX
		global agentPosX
		global agentPosY
		global restOfChipsUser
		global restOfChipsAgent
		if x < 0 or x > 3 or y < 0 or y > 3:
			return 0



		if ChipSet[0]==0 and ChipSet[1]==0 and ChipSet[2]==0 and ChipSet[3]==0 and ChipSet[4]==0:
			return 0


		if previous==1:
			if Grid[x][y].color=='Red':
				if ChipSet[0]>0:
					ChipSet[0]-=1
				else:
					return 0
			elif Grid[x][y].color=='Blue':
				if ChipSet[1]>0:
					ChipSet[1]-=1	
				else:
					return 0
			elif Grid[x][y].color=='Green':
				if ChipSet[2]>0:
					ChipSet[2]-=1	
				else:
					return 0 
			elif Grid[x][y].color=='Yellow':
				if ChipSet[3]>0:
					ChipSet[3]-=1	
				else:
					return 0
			else:
				if ChipSet[4]>0:
					ChipSet[4]-=1	
				else:
					return 0
		else:
			previous=1

		rightChipSet = copy.copy(ChipSet)
		bottomChipSet = copy.copy(ChipSet)
		topChipSet = copy.copy(ChipSet)
		leftChipSet = copy.copy(ChipSet)
		finalChipSet = copy.copy(ChipSet)

		bottomRoute = maxPoints(x,y+1,previous,bottomChipSet,default)
		rightRoute = maxPoints(x+1,y,previous,rightChipSet,default) 
		topRoute = maxPoints(x,y-1,previous,topChipSet,default)
		leftRoute = maxPoints(x-1,y,previous,leftChipSet,default) 

		my_list = []
		if bottomRoute > 0:
			my_list.insert(0,bottomRoute)
		if rightRoute > 0:
			my_list.insert(0,rightRoute)
		if topRoute > 0:
			my_list.insert(0,topRoute)
		if leftRoute > 0:
			my_list.insert(0,leftRoute)

		if len(my_list)>0:
			final = min(my_list)

			if bottomRoute==final:
				finalRoute = maxPoints(x,y+1,previous,finalChipSet,default)

			elif rightRoute==final:
				finalRoute = maxPoints(x+1,y,previous,finalChipSet,default)

			elif leftRoute == final:
				finalRoute = maxPoints(x-1,y,previous,finalChipSet,default)

			elif topRoute == final:
				finalRoute = maxPoints(x,y-1,previous,finalChipSet,default)
			return finalRoute
		else:
			if default=="User":
				userPosX = x
				userPosY = y
				restOfChipsUser=ChipSet
			elif default=="Agent":
				agentPosX = x
				agentPosY = y
				restOfChipsAgent=ChipSet
			return (3-y)+(3-x)



				



	def drawChipsAgent():

		# Drawing ChipSet Agent Start

		#Red Start
		pygame.draw.circle(playSurface,red,(1050,60),30)
		pygame.draw.circle(playSurface,black,(1050,60),30,2)

		#Red End

		#Blue Start

		pygame.draw.circle(playSurface,blue,(1050,140),30)
		pygame.draw.circle(playSurface,black,(1050,140),30,2)

		#Blue End


		#Green Start

		pygame.draw.circle(playSurface,green,(1050,220),30)
		pygame.draw.circle(playSurface,black,(1050,220),30,2)

		#Green End

		#Yellow Start

		pygame.draw.circle(playSurface,yellow,(1050,300),30)
		pygame.draw.circle(playSurface,black,(1050,300),30,2)

		#Yellow End


		#Violet Start

		pygame.draw.circle(playSurface,violet,(1050,380),30)
		pygame.draw.circle(playSurface,black,(1050,380),30,2)

		#Violet End

		#End of ChipSet

	def drawOfferedChipsAgent():
		pygame.draw.circle(playSurface,red,(900,60),30)
		pygame.draw.circle(playSurface,black,(900,60),30,2)

		leftButtonRed = pygame.Rect(820,30,40,60)
		rightButtonRed = pygame.Rect(940,30,40,60)
		pygame.draw.rect(playSurface,grey,leftButtonRed)
		pygame.draw.rect(playSurface,grey,rightButtonRed)
		pygame.draw.rect(playSurface,black,leftButtonRed,1)
		pygame.draw.rect(playSurface,black,rightButtonRed,1)

		myFont = pygame.font.SysFont('monaco', 20)
		GOsurf = myFont.render('<', True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (840,50)
		playSurface.blit(GOsurf,Gorect)
		GOsurf = myFont.render('>', True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (960,50)
		playSurface.blit(GOsurf,Gorect)
		#Red End

		#Blue Start

		pygame.draw.circle(playSurface,blue,(900,140),30)
		pygame.draw.circle(playSurface,black,(900,140),30,2)

		leftButtonRed = pygame.Rect(820,110,40,60)
		rightButtonRed = pygame.Rect(940,110,40,60)
		pygame.draw.rect(playSurface,grey,leftButtonRed)
		pygame.draw.rect(playSurface,grey,rightButtonRed)
		pygame.draw.rect(playSurface,black,leftButtonRed,1)
		pygame.draw.rect(playSurface,black,rightButtonRed,1)

		myFont = pygame.font.SysFont('monaco', 20)
		GOsurf = myFont.render('<', True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (840,130)
		playSurface.blit(GOsurf,Gorect)
		GOsurf = myFont.render('>', True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (960,130)
		playSurface.blit(GOsurf,Gorect)
		#Blue End


		#Green Start


		pygame.draw.circle(playSurface,green,(900,220),30)
		pygame.draw.circle(playSurface,black,(900,220),30,2)

		leftButtonRed = pygame.Rect(820,190,40,60)
		rightButtonRed = pygame.Rect(940,190,40,60)
		pygame.draw.rect(playSurface,grey,leftButtonRed)
		pygame.draw.rect(playSurface,grey,rightButtonRed)
		pygame.draw.rect(playSurface,black,leftButtonRed,1)
		pygame.draw.rect(playSurface,black,rightButtonRed,1)

		myFont = pygame.font.SysFont('monaco', 20)
		GOsurf = myFont.render('<', True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (840,210)
		playSurface.blit(GOsurf,Gorect)
		GOsurf = myFont.render('>', True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (960,210)
		playSurface.blit(GOsurf,Gorect)
		#Green End

		#Yellow Start

		pygame.draw.circle(playSurface,yellow,(900,300),30)
		pygame.draw.circle(playSurface,black,(900,300),30,2)

		leftButtonRed = pygame.Rect(820,270,40,60)
		rightButtonRed = pygame.Rect(940,270,40,60)
		pygame.draw.rect(playSurface,grey,leftButtonRed)
		pygame.draw.rect(playSurface,grey,rightButtonRed)
		pygame.draw.rect(playSurface,black,leftButtonRed,1)
		pygame.draw.rect(playSurface,black,rightButtonRed,1)

		myFont = pygame.font.SysFont('monaco', 20)
		GOsurf = myFont.render('<', True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (840,280)
		playSurface.blit(GOsurf,Gorect)
		GOsurf = myFont.render('>', True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (960,280)
		playSurface.blit(GOsurf,Gorect)
		#Yellow End


		#Violet Start

		pygame.draw.circle(playSurface,violet,(900,380),30)
		pygame.draw.circle(playSurface,black,(900,380),30,2)

		leftButtonRed = pygame.Rect(820,350,40,60)
		rightButtonRed = pygame.Rect(940,350,40,60)
		pygame.draw.rect(playSurface,grey,leftButtonRed)
		pygame.draw.rect(playSurface,grey,rightButtonRed)
		pygame.draw.rect(playSurface,black,leftButtonRed,1)
		pygame.draw.rect(playSurface,black,rightButtonRed,1)

		myFont = pygame.font.SysFont('monaco', 20)
		GOsurf = myFont.render('<', True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (840,370)
		playSurface.blit(GOsurf,Gorect)
		GOsurf = myFont.render('>', True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (960,370)
		playSurface.blit(GOsurf,Gorect)
		#Violet End


		#End of ChipSet

	def drawOfferedChipsUser():

		# Drawing ChipSet Agent Start

		#Red Start
		pygame.draw.circle(playSurface,red,(650,60),30)
		pygame.draw.circle(playSurface,black,(650,60),30,2)

		#Red End

		#Blue Start

		pygame.draw.circle(playSurface,blue,(650,140),30)
		pygame.draw.circle(playSurface,black,(650,140),30,2)

		#Blue End


		#Green Start

		pygame.draw.circle(playSurface,green,(650,220),30)
		pygame.draw.circle(playSurface,black,(650,220),30,2)

		#Green End

		#Yellow Start

		pygame.draw.circle(playSurface,yellow,(650,300),30)
		pygame.draw.circle(playSurface,black,(650,300),30,2)

		#Yellow End


		#Violet Start

		pygame.draw.circle(playSurface,violet,(650,380),30)
		pygame.draw.circle(playSurface,black,(650,380),30,2)

		#Violet End

		#End of ChipSet


	def drawChips():
		pygame.draw.circle(playSurface,red,(500,60),30)
		pygame.draw.circle(playSurface,black,(500,60),30,2)

		
		#Red End

		#Blue Start

		pygame.draw.circle(playSurface,blue,(500,140),30)
		pygame.draw.circle(playSurface,black,(500,140),30,2)

		
		#Blue End


		#Green Start


		pygame.draw.circle(playSurface,green,(500,220),30)
		pygame.draw.circle(playSurface,black,(500,220),30,2)

		
		#Green End

		#Yellow Start

		pygame.draw.circle(playSurface,yellow,(500,300),30)
		pygame.draw.circle(playSurface,black,(500,300),30,2)

		
		#Yellow End


		#Violet Start

		pygame.draw.circle(playSurface,violet,(500,380),30)
		pygame.draw.circle(playSurface,black,(500,380),30,2)

		
		#Violet End


		#End of ChipSet


	def showButtonsChips():
		leftButtonRed = pygame.Rect(420,30,40,60)
		rightButtonRed = pygame.Rect(540,30,40,60)
		pygame.draw.rect(playSurface,grey,leftButtonRed)
		pygame.draw.rect(playSurface,grey,rightButtonRed)
		pygame.draw.rect(playSurface,black,leftButtonRed,1)
		pygame.draw.rect(playSurface,black,rightButtonRed,1)

		myFont = pygame.font.SysFont('monaco', 20)
		GOsurf = myFont.render('<', True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (440,50)
		playSurface.blit(GOsurf,Gorect)
		GOsurf = myFont.render('>', True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (560,50)
		playSurface.blit(GOsurf,Gorect)
		leftButtonRed = pygame.Rect(420,110,40,60)
		rightButtonRed = pygame.Rect(540,110,40,60)
		pygame.draw.rect(playSurface,grey,leftButtonRed)
		pygame.draw.rect(playSurface,grey,rightButtonRed)
		pygame.draw.rect(playSurface,black,leftButtonRed,1)
		pygame.draw.rect(playSurface,black,rightButtonRed,1)

		myFont = pygame.font.SysFont('monaco', 20)
		GOsurf = myFont.render('<', True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (440,130)
		playSurface.blit(GOsurf,Gorect)
		GOsurf = myFont.render('>', True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (560,130)
		playSurface.blit(GOsurf,Gorect)
		leftButtonRed = pygame.Rect(420,190,40,60)
		rightButtonRed = pygame.Rect(540,190,40,60)
		pygame.draw.rect(playSurface,grey,leftButtonRed)
		pygame.draw.rect(playSurface,grey,rightButtonRed)
		pygame.draw.rect(playSurface,black,leftButtonRed,1)
		pygame.draw.rect(playSurface,black,rightButtonRed,1)

		myFont = pygame.font.SysFont('monaco', 20)
		GOsurf = myFont.render('<', True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (440,210)
		playSurface.blit(GOsurf,Gorect)
		GOsurf = myFont.render('>', True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (560,210)
		playSurface.blit(GOsurf,Gorect)
		leftButtonRed = pygame.Rect(420,270,40,60)
		rightButtonRed = pygame.Rect(540,270,40,60)
		pygame.draw.rect(playSurface,grey,leftButtonRed)
		pygame.draw.rect(playSurface,grey,rightButtonRed)
		pygame.draw.rect(playSurface,black,leftButtonRed,1)
		pygame.draw.rect(playSurface,black,rightButtonRed,1)

		myFont = pygame.font.SysFont('monaco', 20)
		GOsurf = myFont.render('<', True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (440,290)
		playSurface.blit(GOsurf,Gorect)
		GOsurf = myFont.render('>', True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (560,290)
		playSurface.blit(GOsurf,Gorect)
		leftButtonRed = pygame.Rect(420,350,40,60)
		rightButtonRed = pygame.Rect(540,350,40,60)
		pygame.draw.rect(playSurface,grey,leftButtonRed)
		pygame.draw.rect(playSurface,grey,rightButtonRed)
		pygame.draw.rect(playSurface,black,leftButtonRed,1)
		pygame.draw.rect(playSurface,black,rightButtonRed,1)

		myFont = pygame.font.SysFont('monaco', 20)
		GOsurf = myFont.render('<', True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (440,370)
		playSurface.blit(GOsurf,Gorect)
		GOsurf = myFont.render('>', True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (560,370)
		playSurface.blit(GOsurf,Gorect)

	def shuffleChipSet(agentChipSet,userChipSet):
		numberOfShuffles = random.randrange(1,5)
		if roundNumber%10==4 or roundNumber%10==5 or roundNumber%10==9 or roundNumber%10==6:
			numberOfShuffles = random.randrange(1,3)
		for x in range(0,numberOfShuffles):
			shuffle= True 
			shuffleType = random.randrange(0,13)
			if roundNumber%10==4 or roundNumber%10 == 6:
				shuffleType=5
			elif roundNumber%10==5 or roundNumber%10==9:
				shuffleType=7
			
			while shuffle:

				if shuffleType<5:
					randomAgent = random.randrange(0,5)
					randomUser = random.randrange(0,5)
					if agentChipSet[randomAgent]>0 and userChipSet[randomUser]>0:
						agentChipSet[randomAgent]-=1
						agentChipSet[randomUser]+=1
						userChipSet[randomUser]-=1
						userChipSet[randomAgent]+=1
						shuffle=False
				elif shuffleType<7:
					randomUser = random.randrange(0,5)
					if  userChipSet[randomUser]>0:
						agentChipSet[randomUser]+=1
						userChipSet[randomUser]-=1
						shuffle=False
				elif shuffleType<9:
					randomAgent = random.randrange(0,5)
					if agentChipSet[randomAgent]>0:
						agentChipSet[randomAgent]-=1
						userChipSet[randomAgent]+=1
						shuffle=False
				elif shuffleType==9:
					randomAgent = random.randrange(0,5)
					if agentChipSet[randomAgent]>0:
						agentChipSet[randomAgent]-=1
						shuffle=False
				elif shuffleType==10:
					randomUser = random.randrange(0,5)
					if  userChipSet[randomUser]>0:
						userChipSet[randomUser]-=1
						shuffle=False
				elif shuffleType==11:
					randomAgent = random.randrange(0,5)
					agentChipSet[randomAgent]+=1
					shuffle=False
				elif shuffleType==12:
					randomUser = random.randrange(0,5)
					userChipSet[randomUser]+=1
					shuffle=False		

	# #User and Agent Positions Randomly Assigned Start
	if roundNumber%10 != 7:
		userPosX = random.randrange(0,2)
		userPosY = random.randrange(0,2)
		userPosXCSV=copy.copy(userPosX)
		userPosYCSV=copy.copy(userPosY)

		Grid[userPosX][userPosY].state="User1"
		randomChipSet = random.randrange(1,5)
		if randomChipSet==1:
			updateRequiredPath1User(userPosX,userPosY)
			userChipSet=requiredPathUser[0]
		elif randomChipSet==2:
			updateRequiredPath2User(userPosX,userPosY)
			userChipSet=requiredPathUser[1]
		elif randomChipSet ==3:
			updateRequiredPath3User(userPosX,userPosY)
			userChipSet=requiredPathUser[2]
		else:
			updateRequiredPath4User(userPosX,userPosY)
			userChipSet=requiredPathUser[3]


		if roundNumber%2==0:
			remainingChipSetUser=copy.copy(userChipSet)	
		else:
			# agentChipSet=requiredPathUser[3]
			remainingChipSetAgent=copy.copy(userChipSet)
			
		# pprint(userChipSet)

		while True:
			agentPosX = random.randrange(0,2)
			agentPosY = random.randrange(0,2)
			agentPosXCSV=copy.copy(agentPosX)
			agentPosYCSV=copy.copy(agentPosY)

			if Grid[agentPosX][agentPosY].state!="User1":
				Grid[agentPosX][agentPosY].state="Agent"
				randomChipSet = random.randrange(1,5)
				if randomChipSet==1:
					updateRequiredPath1Agent(agentPosX,agentPosY)
					agentChipSet=requiredPathAgent[0]
				elif randomChipSet==2:
					updateRequiredPath2Agent(agentPosX,agentPosY)
					agentChipSet=requiredPathAgent[1]
				elif randomChipSet ==3:
					updateRequiredPath3Agent(agentPosX,agentPosY)
					agentChipSet=requiredPathAgent[2]
				else:
					updateRequiredPath4Agent(agentPosX,agentPosY)
					agentChipSet=requiredPathAgent[3]

				if roundNumber%2==0:
					
					remainingChipSetAgent=copy.copy(agentChipSet)
					ChipSetAgentBeginning = copy.copy(remainingChipSetAgent)
				else:
					# userChipSet=requiredPathAgent[2]
					remainingChipSetUser=copy.copy(agentChipSet)
					ChipSetUserBeginning = copy.copy(remainingChipSetUser)	
				break
			else:
				print("Position Taken")
		shuffleChipSet(remainingChipSetAgent,remainingChipSetUser)
	ChipSetUserBeginning = copy.copy(remainingChipSetUser)
	ChipSetAgentBeginning = copy.copy(remainingChipSetAgent)
	#Positions End

	#Game over function 



	def drawGrid():
		for y in range(0, 4):
			for x in range(0,4):
				Rect = pygame.Rect(50+(x*80),30+(y*80),80,80)
				pygame.draw.rect(playSurface,black,Rect,5)

				if Grid[x][y].color == 'Blue':
					pygame.draw.rect(playSurface,blue,Rect)
				elif Grid[x][y].color == 'Yellow':
					pygame.draw.rect(playSurface,yellow,Rect)
				elif Grid[x][y].color == 'Red':
					pygame.draw.rect(playSurface,red,Rect)	
				elif Grid[x][y].color == 'Violet':
					pygame.draw.rect(playSurface,violet,Rect)
				else:
					pygame.draw.rect(playSurface,green,Rect)

	
					
		textPosX = 90+(userPosX*80)
		textPosY = 35+(userPosY*80)

		myFont = pygame.font.SysFont('monaco', 25)
		GOsurf = myFont.render('User 1', True, black)
		Gorect = GOsurf.get_rect()

		Gorect.midtop = (textPosX,textPosY)
		playSurface.blit(GOsurf,Gorect)

		textPosX = 90+(agentPosX*80)
		textPosY = 80+(agentPosY*80)

		myFont = pygame.font.SysFont('monaco', 25)
		GOsurf = myFont.render('Agent', True, black)
		Gorect = GOsurf.get_rect()
		Gorect.midtop = (textPosX,textPosY)
		playSurface.blit(GOsurf,Gorect)
					
		textPosX = 90+(3*80)
		textPosY = 60+(3*80)

		myFont = pygame.font.SysFont('monaco', 25)
		GOsurf = myFont.render('GOAL', True, black)
		Gorect = GOsurf.get_rect()
		Gorect.midtop = (textPosX,textPosY)
		playSurface.blit(GOsurf,Gorect)
				
		myFont = pygame.font.SysFont('monaco', 35)
		
		GOsurf = myFont.render(' Round {0}  '.format(roundNumber+1), True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (650,570)
		playSurface.blit(GOsurf,scorerect)



	#User Chipset
	def showChipSet():
		myFont = pygame.font.SysFont('monaco', 35)
		
		GOsurf = myFont.render('  {0}  '.format(remainingChipSetUser[0]), True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (500,50)
		playSurface.blit(GOsurf,scorerect)

		GOsurf = myFont.render('  {0}  '.format(remainingChipSetUser[1]), True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (500,130)
		playSurface.blit(GOsurf,scorerect)

		GOsurf = myFont.render('  {0}  '.format(remainingChipSetUser[2]), True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (500,210)
		playSurface.blit(GOsurf,scorerect)

		GOsurf = myFont.render('  {0}  '.format(remainingChipSetUser[3]), True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (500,290)
		playSurface.blit(GOsurf,scorerect)

		GOsurf = myFont.render('  {0}  '.format(remainingChipSetUser[4]), True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (500,370)
		playSurface.blit(GOsurf,scorerect)

		myFont = pygame.font.SysFont('monaco', 20)	
		GOsurf = myFont.render('Your', True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (500,415)
		playSurface.blit(GOsurf,scorerect)
		GOsurf = myFont.render('Chips', True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (500,430)
		playSurface.blit(GOsurf,scorerect)



	#Agent Chipset
	def showChipSetAgent():
		myFont = pygame.font.SysFont('monaco', 35)
		
		GOsurf = myFont.render('  {0}  '.format(remainingChipSetAgent[0]), True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (1050,50)
		playSurface.blit(GOsurf,scorerect)

		GOsurf = myFont.render('  {0}  '.format(remainingChipSetAgent[1]), True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (1050,130)
		playSurface.blit(GOsurf,scorerect)

		GOsurf = myFont.render('  {0}  '.format(remainingChipSetAgent[2]), True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (1050,210)
		playSurface.blit(GOsurf,scorerect)

		GOsurf = myFont.render('  {0}  '.format(remainingChipSetAgent[3]), True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (1050,290)
		playSurface.blit(GOsurf,scorerect)

		GOsurf = myFont.render('  {0}  '.format(remainingChipSetAgent[4]), True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (1050,370)
		playSurface.blit(GOsurf,scorerect)

		myFont = pygame.font.SysFont('monaco', 20)	
		GOsurf = myFont.render("Agent's", True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (1050,415)
		playSurface.blit(GOsurf,scorerect)
		GOsurf = myFont.render('Chips', True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (1050,430)
		playSurface.blit(GOsurf,scorerect)

	#Offered Chipset Agent
	def showOfferedChipSetAgent():
		myFont = pygame.font.SysFont('monaco', 35)
		
		GOsurf = myFont.render('  {0}  '.format(offeredChipSetAgent[0]), True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (900,50)
		playSurface.blit(GOsurf,scorerect)

		GOsurf = myFont.render('  {0}  '.format(offeredChipSetAgent[1]), True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (900,130)
		playSurface.blit(GOsurf,scorerect)

		GOsurf = myFont.render('  {0}  '.format(offeredChipSetAgent[2]), True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (900,210)
		playSurface.blit(GOsurf,scorerect)

		GOsurf = myFont.render('  {0}  '.format(offeredChipSetAgent[3]), True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (900,290)
		playSurface.blit(GOsurf,scorerect)

		GOsurf = myFont.render('  {0}  '.format(offeredChipSetAgent [4]), True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (900,370)
		playSurface.blit(GOsurf,scorerect)

		myFont = pygame.font.SysFont('monaco', 20)	
		GOsurf = myFont.render("Agent's", True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (900,415)
		playSurface.blit(GOsurf,scorerect)
		GOsurf = myFont.render('Offered', True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (900,430)
		playSurface.blit(GOsurf,scorerect)
		GOsurf = myFont.render('Chips', True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (900,445)
		playSurface.blit(GOsurf,scorerect)



	#Show Offered ChipSet User

	def showResponse():
		

		myFont = pygame.font.SysFont('monaco', 30)
		GOsurf = myFont.render('', True, red)

		if agentAccepted==1:
			GOsurf = myFont.render('The Agent Accepted', True, red)
		elif agentAccepted==0:
			GOsurf = myFont.render('The Agent Rejected', True, red)
		elif agentAccepted==4:
			myFont = pygame.font.SysFont('monaco', 30)

			Rect = pygame.Rect(800,500,100,40)
			pygame.draw.rect(playSurface,black,Rect,5)
			pygame.draw.rect(playSurface,grey,Rect)

			GOsurf = myFont.render('Accept', True, black)
			Gorect = GOsurf.get_rect()
			Gorect.midtop = (850,510)
			playSurface.blit(GOsurf,Gorect)

			Rect = pygame.Rect(925,500,100,40)
			pygame.draw.rect(playSurface,black,Rect,5)
			pygame.draw.rect(playSurface,grey,Rect)

			GOsurf = myFont.render('Reject', True, black)
			Gorect = GOsurf.get_rect()
			Gorect.midtop = (975,510)
			playSurface.blit(GOsurf,Gorect)
			GOsurf = myFont.render('The Agent Sent An Offer', True, red)
		if PersistanceFlag==1:
			GOsurf = myFont.render('Try Another Time', True, red)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (800,470)
		playSurface.blit(GOsurf,scorerect)

		myFont = pygame.font.SysFont('monaco', 25)

		GOsurf = myFont.render('{0}'.format(hintText), True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (800,530)
		playSurface.blit(GOsurf,scorerect)




	#Show Offered ChipSet User
	def showOfferedChipSetUser():
		myFont = pygame.font.SysFont('monaco', 35)
		
		GOsurf = myFont.render('  {0}  '.format(offeredChipSetUser[0]), True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (650,50)
		playSurface.blit(GOsurf,scorerect)

		GOsurf = myFont.render('  {0}  '.format(offeredChipSetUser[1]), True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (650,130)
		playSurface.blit(GOsurf,scorerect)

		GOsurf = myFont.render('  {0}  '.format(offeredChipSetUser[2]), True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (650,210)
		playSurface.blit(GOsurf,scorerect)

		GOsurf = myFont.render('  {0}  '.format(offeredChipSetUser[3]), True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (650,290)
		playSurface.blit(GOsurf,scorerect)

		GOsurf = myFont.render('  {0}  '.format(offeredChipSetUser[4]), True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (650,370)
		playSurface.blit(GOsurf,scorerect)

		myFont = pygame.font.SysFont('monaco', 20)	
		GOsurf = myFont.render('Your', True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (650,415)
		playSurface.blit(GOsurf,scorerect)
		GOsurf = myFont.render('Offered', True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (650,430)
		playSurface.blit(GOsurf,scorerect)
		GOsurf = myFont.render('Chips', True, black)
		scorerect = GOsurf.get_rect()
		scorerect.midtop = (650,445)
		playSurface.blit(GOsurf,scorerect)

	# Main Logic of the game 

	while True:

		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				pygame.quit()
				sys.exit()
			elif event.type == pygame.KEYDOWN:
				if event.key == pygame.K_ESCAPE:
					pygame.event.post(pygame.event.Event('QUIT'))
			elif event.type == pygame.MOUSEBUTTONDOWN:
				pos = pygame.mouse.get_pos()

				#Agent ChipSet Offer
				if (pos[0]<860 and pos[0]>820):
					if pos[1] < 90 and pos[1] > 30:
						clicksAgent+=1
						if remainingChipSetAgent[0]>0:

							offeredChipSetAgent[0]+=1
							remainingChipSetAgent[0]-=1

					elif pos[1] < 170 and pos[1] > 110:	
						clicksAgent+=1
						if remainingChipSetAgent[1]>0:
							offeredChipSetAgent[1]+=1
							remainingChipSetAgent[1]-=1

					elif pos[1] < 250 and pos[1] > 190:
						clicksAgent+=1
						if remainingChipSetAgent[2]>0:
							offeredChipSetAgent[2]+=1
							remainingChipSetAgent[2]-=1					
	
					elif pos[1] < 330 and pos[1] > 270:
						clicksAgent+=1
						if remainingChipSetAgent[3]>0:
							offeredChipSetAgent[3]+=1
							remainingChipSetAgent[3]-=1
						
					elif pos[1] < 410 and pos[1] > 350:
						clicksAgent+=1
						if remainingChipSetAgent[4]>0:
							offeredChipSetAgent[4]+=1
							remainingChipSetAgent[4]-=1
						# decreaseChipSet('Violet') 

				elif (pos[0]<980 and pos[0]>940):
					if pos[1] < 90 and pos[1] > 30:
						clicksAgent+=1
						if remainingChipSetAgent[0]<agentChipSet[0]:

							offeredChipSetAgent[0]-=1
							remainingChipSetAgent[0]+=1

					elif pos[1] < 170 and pos[1] > 110:	
						clicksAgent+=1
						if remainingChipSetAgent[1]<agentChipSet[1]:
							offeredChipSetAgent[1]-=1
							remainingChipSetAgent[1]+=1
							# print offeredChipSetAgent[1]
							# print remainingChipSetAgent[1]
						# decreaseChipSet('Blue')
					elif pos[1] < 250 and pos[1] > 190:
						clicksAgent+=1
						if remainingChipSetAgent[2]<agentChipSet[2]:
							offeredChipSetAgent[2]-=1
							remainingChipSetAgent[2]+=1					
						# decreaseChipSet('Green')
					elif pos[1] < 330 and pos[1] > 270:
						clicksAgent+=1
						if remainingChipSetAgent[3]<agentChipSet[3]:
							offeredChipSetAgent[3]-=1
							remainingChipSetAgent[3]+=1
						# decreaseChipSet('Yellow')
					elif pos[1] < 410 and pos[1] > 350:
						clicksAgent+=1
						if remainingChipSetAgent[4]<agentChipSet[4]:
							offeredChipSetAgent[4]-=1
							remainingChipSetAgent[4]+=1
						# decreaseChipSet('Violet')

				#User
				 	#ChipSet 
				 		#Offer
				elif (pos[0]<580 and pos[0]>540):
					if pos[1] < 90 and pos[1] > 30:
						clicksUser+=1
						if remainingChipSetUser[0]>0:

							offeredChipSetUser[0]+=1
							remainingChipSetUser[0]-=1
							# print offeredChipSetAgent[0]
							# print remainingChipSetAgent[0]
						# decreaseChipSet('Red')
					elif pos[1] < 170 and pos[1] > 110:	
						clicksUser+=1
						if remainingChipSetUser[1]>0:
							offeredChipSetUser[1]+=1
							remainingChipSetUser[1]-=1
							# print offeredChipSetAgent[1]
							# print remainingChipSetAgent[1]
						# decreaseChipSet('Blue')
					elif pos[1] < 250 and pos[1] > 190:
						clicksUser+=1
						if remainingChipSetUser[2]>0:
							offeredChipSetUser[2]+=1
							remainingChipSetUser[2]-=1					
						# decreaseChipSet('Green')
					elif pos[1] < 330 and pos[1] > 270:
						clicksUser+=1
						if remainingChipSetUser[3]>0:
							offeredChipSetUser[3]+=1
							remainingChipSetUser[3]-=1
						# decreaseChipSet('Yellow')
					elif pos[1] < 410 and pos[1] > 350:
						clicksUser+=1
						if remainingChipSetUser[4]>0:
							offeredChipSetUser[4]+=1
							remainingChipSetUser[4]-=1
						# decreaseChipSet('Violet') 

				elif (pos[0]<460 and pos[0]>420):

					if pos[1] < 90 and pos[1] > 30:
						clicksUser+=1
						if remainingChipSetUser[0]<userChipSet[0]:

							offeredChipSetUser[0]-=1
							remainingChipSetUser[0]+=1

					elif pos[1] < 170 and pos[1] > 110:	
						clicksUser+=1
						if remainingChipSetUser[1]<userChipSet[1]:
							offeredChipSetUser[1]-=1
							remainingChipSetUser[1]+=1
							# print offeredChipSetAgent[1]
							# print remainingChipSetAgent[1]
						# decreaseChipSet('Blue')
					elif pos[1] < 250 and pos[1] > 190:
						clicksUser+=1
						if remainingChipSetUser[2]<userChipSet[2]:
							offeredChipSetUser[2]-=1
							remainingChipSetUser[2]+=1					
						# decreaseChipSet('Green')
					elif pos[1] < 330 and pos[1] > 270:
						clicksUser+=1
						if remainingChipSetUser[3]<userChipSet[3]:
							offeredChipSetUser[3]-=1
							remainingChipSetUser[3]+=1
						# decreaseChipSet('Yellow')
					elif pos[1] < 410 and pos[1] > 350:
						clicksUser+=1
						if remainingChipSetUser[4]<userChipSet[4]:
							offeredChipSetUser[4]-=1
							remainingChipSetUser[4]+=1
						# decreaseChipSet('Violet') 
				elif pos[0]>50 and pos[0]<250:
					if pos[1]<540 and pos[1]>500:
						if userTurn==1:
							offeredChipSetAgentCopy= copy.copy(offeredChipSetAgent)
							offeredChipSetUserCopy= copy.copy(offeredChipSetUser)
							
							restOfChipsUser = copy.copy(remainingChipSetUser)
							restOfChipsAgent = copy.copy(remainingChipSetAgent)

							testRemainingUser = copy.copy(remainingChipSetUser)
							testOfferedUser = copy.copy(offeredChipSetAgent)

							testRemainingAgent = copy.copy(remainingChipSetAgent)
							testOfferedAgent = copy.copy(offeredChipSetUser)

							userPosXTestStart = copy.copy(userPosX)
							userPosYTestStart = copy.copy(userPosY)

							userPosXTestPost = copy.copy(userPosX)
							userPosYTestPost = copy.copy(userPosY)

							agentPosXTestStart = copy.copy(agentPosX)
							agentPosYTestStart = copy.copy(agentPosY)

							agentPosXTestPost = copy.copy(agentPosX)
							agentPosYTestPost = copy.copy(agentPosY)


							route=[]

							BeginningUser = analyzeChipSet(userPosXTestStart,userPosYTestStart,0,ChipSetUserBeginning,"User")

							if BeginningUser==1:
								restOfChipsUser = getChipSetAfterRoute(userPosXTestStart,userPosYTestStart,restOfChipsUser)
								points = 0	
							else:
								points = maxPoints(userPosXTestStart,userPosYTestStart,0,restOfChipsUser,"User")

		
							countRemainingChips=sum(restOfChipsUser)
							BeginningPointUser = 125 + countRemainingChips*10 - 25*points
							pprint(BeginningPointUser)


							route=[]
							BeginningAgent = analyzeChipSet(agentPosXTestStart,agentPosYTestStart,0,ChipSetAgentBeginning,"Agent")
							if BeginningAgent==1:
								restOfChipsAgent = getChipSetAfterRoute(agentPosXTestStart,agentPosYTestStart,restOfChipsAgent)
								points = 0	
							else:
								points = maxPoints(agentPosXTestStart,agentPosYTestStart,0,restOfChipsAgent,"Agent")

							countRemainingChips=sum(restOfChipsAgent)
							BeginningPointAgent = 125 + countRemainingChips*10 - 25*points
							pprint(BeginningPointAgent)
							pprint("post")
							route=[]

							PostOfferStatusUser = analyzeOffer(userPosXTestPost,userPosYTestPost,testRemainingUser,testOfferedUser)

							if PostOfferStatusUser==1:
								restOfChipsUser = getChipSetAfterRoute(userPosXTestPost,userPosYTestPost,
									map(operator.add, testRemainingUser,testOfferedUser))
								userPosX = 3
								userPosY = 3
								points = 0	
							else:
								points = maxPoints(userPosXTestPost,userPosYTestPost,0,map(operator.add, testRemainingUser,testOfferedUser),"User")
							countRemainingChips=sum(restOfChipsUser)
							PostPointUser = 125 + countRemainingChips*10 - 25*points

	

							
							route=[]
							PostOfferStatusAgent = analyzeOffer(agentPosXTestPost,agentPosYTestPost,testRemainingAgent,testOfferedAgent)
							if PostOfferStatusAgent==1:
								restOfChipsAgent = getChipSetAfterRoute(agentPosXTestPost,agentPosYTestPost,map(operator.add, testOfferedAgent,testRemainingAgent))
								
								agentPosX = 3
								agentPosY = 3
								points = 0	
							else:
								points = maxPoints(agentPosXTestPost,agentPosYTestPost,0,map(operator.add, testRemainingAgent,testOfferedAgent),"Agent")
								

							countRemainingChips=sum(restOfChipsAgent)
							PostPointAgent = 125 + countRemainingChips*10 - 25*points
							if PostPointAgent > BeginningPointAgent:
								randomAI = random.randrange(0,16)
								if randomAI>2:
									if PostPointUser > BeginningPointUser:
										if PostPointUser > PostPointAgent:
											hintText = "Clever Offer!"
										elif PostPointUser < PostPointAgent:
											hintText = "Good thinking, but not good enough"
										else:
											hintText = "Playing it safe"
									elif PostPointUser < BeginningPointUser:
										if PostPointUser > PostPointAgent:
											hintText = "You could've had more points"
										elif PostPointUser < PostPointAgent:
											hintText = "Are you trying to help him win ?"
										else:
											hintText = "What a nice gesture !"
									

									postResponsePointsUser = PostPointUser
									postResponseStatusUser = PostOfferStatusUser
									postResponseStatusAgent = PostOfferStatusAgent
									postResponsePointsAgent = PostPointAgent
									agentAccepted=1
								else :
									if PostPointUser>BeginningPointUser:
										if PostPointUser > PostPointAgent:
											hintText = "Better luck next round"
										elif PostPointUser < PostPointAgent:
											hintText = "Good thinking, but not good enough"	

									elif PostPointUser < BeginningPointUser:
										if PostPointUser < PostPointAgent:
											hintText = "Are you trying to help him win ?"
									else:
										if PostPointUser > PostPointAgent:
											if sum(offeredChipSetUserCopy)==0 and sum(offeredChipSetAgentCopy)>0 :
												hintText = "You're being greedy"
											elif sum(offeredChipSetUserCopy)==0 and sum(offeredChipSetAgentCopy)==0:
												hintText = "Nice thinking not making an offer"
									postResponsePointsUser = BeginningPointUser
									postResponsePointsAgent = BeginningPointAgent
									postResponseStatusUser = BeginningUser
									postResponseStatusAgent = BeginningAgent
									agentAccepted=0
							else:
								randomAI = random.randrange(0,16)
								if randomAI<2:
									if PostPointUser>BeginningPointUser:
										if PostPointUser > PostPointAgent:
											hintText = "Agent decided to cooperate and let you win"
										elif PostPointUser < PostPointAgent:
											hintText = "You didnt think this through"
										else:
											hintText = "Clever Offer"
									else :
										hintText = "You should've considered not making an offer"
									postResponsePointsUser = PostPointUser
									postResponseStatusUser = PostOfferStatusUser
									postResponseStatusAgent = PostOfferStatusAgent
									postResponsePointsAgent = PostPointAgent
									agentAccepted=1
								else :
									if PostPointUser<BeginningPointUser:
										if BeginningPointUser > BeginningPointAgent:
											hintText = "Agent decided to help"
										else:
											hintText = "Better luck next round"
									else:
										if sum(offeredChipSetUser)>0:
											hintText = "You didnt think this through"

									postResponsePointsUser = BeginningPointUser
									postResponsePointsAgent = BeginningPointAgent
									postResponseStatusUser = BeginningUser
									postResponseStatusAgent = BeginningAgent
									agentAccepted=0

							if roundNumber%10==4 or roundNumber%10==6  :
								postResponsePointsUser = BeginningPointUser
								postResponsePointsAgent = BeginningPointAgent
								postResponseStatusUser = BeginningUser
								postResponseStatusAgent = BeginningAgent
								if sum(offeredChipSetAgentCopy)>0:
									hintText = "The Agent doesnt want to help"
								agentAccepted=0

							pprint(PostPointUser)
							pprint(PostPointAgent)
							elapsedTime = time.time()- startTime
							userTurn=3
							break
						elif userTurn==2:
							offeredChipSetAgentCopy= copy.copy(offeredChipSetAgent)
							offeredChipSetUserCopy= copy.copy(offeredChipSetUser)
							
							restOfChipsUser = copy.copy(remainingChipSetUser)
							restOfChipsAgent = copy.copy(remainingChipSetAgent)



							userPosXTestStart = copy.copy(userPosX)
							userPosYTestStart = copy.copy(userPosY)

							userPosXTestPost = copy.copy(userPosX)
							userPosYTestPost = copy.copy(userPosY)

							agentPosXTestStart = copy.copy(agentPosX)
							agentPosYTestStart = copy.copy(agentPosY)

							agentPosXTestPost = copy.copy(agentPosX)
							agentPosYTestPost = copy.copy(agentPosY)
							
							route=[]

							BeginningUser = analyzeChipSet(userPosXTestStart,userPosYTestStart,0,ChipSetUserBeginning,"User")

							if BeginningUser==1:
								restOfChipsUser = getChipSetAfterRoute(userPosXTestStart,userPosYTestStart,restOfChipsUser)
								userPosX = 3
								userPosY = 3
								points = 0	
							else:
								points = maxPoints(userPosXTestStart,userPosYTestStart,0,restOfChipsUser,"User")
							rejectXUser = copy.copy(userPosX)
							rejectYUser = copy.copy(userPosY)
							userPosX = copy.copy(userPosXCSV)
							userPosY = copy.copy(userPosYCSV)

							countRemainingChips=sum(restOfChipsUser)
							BeginningPointUser = 125 + countRemainingChips*10 - 25*points
							pprint(BeginningPointUser)


							route=[]
							BeginningAgent = analyzeChipSet(agentPosXTestStart,agentPosYTestStart,0,ChipSetAgentBeginning,"Agent")
							if BeginningAgent==1:
								restOfChipsAgent = getChipSetAfterRoute(agentPosXTestStart,agentPosYTestStart,restOfChipsAgent)
								agentPosX = 3
								agentPosY = 3
								points = 0	
							else:
								points = maxPoints(agentPosXTestStart,agentPosYTestStart,0,restOfChipsAgent,"Agent")
							rejectXAgent = copy.copy(agentPosX)
							rejectYAgent = copy.copy(agentPosY)
							agentPosX = copy.copy(agentPosXCSV)
							agentPosY = copy.copy(agentPosYCSV)
							countRemainingChips=sum(restOfChipsAgent)
							BeginningPointAgent = 125 + countRemainingChips*10 - 25*points
							pprint(BeginningPointAgent)
							pprint("post")
							
							


							makeAnOffer(agentPosX,agentPosY,remainingChipSetAgent,remainingChipSetUser)

							testRemainingUser = copy.copy(remainingChipSetUser)
							testOfferedUser = copy.copy(offeredChipSetAgent)

							testRemainingAgent = copy.copy(remainingChipSetAgent)
							testOfferedAgent = copy.copy(offeredChipSetUser)

							route=[]

							PostOfferStatusUser = analyzeOffer(userPosXTestPost,userPosYTestPost,testRemainingUser,testOfferedUser)

							if PostOfferStatusUser==1:
								restOfChipsUser = getChipSetAfterRoute(userPosXTestPost,userPosYTestPost,
									map(operator.add, testRemainingUser,testOfferedUser))
								userPosX = 3
								userPosY = 3
								points = 0	
							else:
								points = maxPoints(userPosXTestPost,userPosYTestPost,0,map(operator.add, testRemainingUser,testOfferedUser),"User")
							acceptXUser = copy.copy(userPosX)
							acceptYUser = copy.copy(userPosY)

							userPosX = copy.copy(userPosXCSV)
							userPosY = copy.copy(userPosYCSV)

							countRemainingChips=sum(restOfChipsUser)
							PostPointUser = 125 + countRemainingChips*10 - 25*points

	

							
							route=[]
							PostOfferStatusAgent = analyzeOffer(agentPosXTestPost,agentPosYTestPost,testRemainingAgent,testOfferedAgent)
							if PostOfferStatusAgent==1:
								restOfChipsAgent = getChipSetAfterRoute(agentPosXTestPost,agentPosYTestPost,map(operator.add, testOfferedAgent,testRemainingAgent))
								agentPosX = 3
								agentPosY =3
								points = 0	
							else:
								points = maxPoints(agentPosXTestPost,agentPosYTestPost,0,map(operator.add, testRemainingAgent,testOfferedAgent),"Agent")
							
							acceptXAgent = copy.copy(agentPosX)
							acceptYAgent = copy.copy(agentPosY)

							agentPosX = copy.copy(agentPosXCSV)
							agentPosY = copy.copy(agentPosYCSV)

							countRemainingChips=sum(restOfChipsAgent)
							PostPointAgent = 125 + countRemainingChips*10 - 25*points

							pprint(PostPointUser)
							pprint(PostPointAgent)

							

							userTurn=3
							agentAccepted=4
							break

		if userTurn==3:
			break
		playSurface.fill(white)
		drawGrid()
		drawChips()
		showChipSet()
		drawChipsAgent()
		if userTurn==1:
			showButtonsChips()
			drawOfferedChipsAgent()
			drawOfferedChipsUser()
			showOfferedChipSetAgent()
			showOfferedChipSetUser()
		drawButtons()
		showChipSetAgent()
		
		showResponse()
		
		pygame.display.flip()
		fpsController.tick(50)
	while True:

		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				pygame.quit()
				sys.exit()
			elif event.type == pygame.KEYDOWN:
				if event.key == pygame.K_ESCAPE:
					pygame.event.post(pygame.event.Event('QUIT'))
			elif event.type == pygame.MOUSEBUTTONDOWN:
				pos = pygame.mouse.get_pos()
				if pos[0]>50 and pos[0]<250:
					if pos[1]<540 and pos[1]>500:
						if NextRound == 1:
							file.writerow([offeredChipSetUserCopy,offeredChipSetAgentCopy,agentAccepted,
								userAccepted,[userPosXCSV,userPosYCSV],[agentPosXCSV,agentPosYCSV],
								ChipSetUserBeginning,ChipSetAgentBeginning,
								BeginningUser,BeginningAgent
								, PostOfferStatusUser, PostOfferStatusAgent
								, BeginningPointUser, BeginningPointAgent
								, PostPointUser, PostPointAgent
								, postResponseStatusUser,postResponseStatusAgent
								, postResponsePointsUser,postResponsePointsAgent
								, restOfChipsUser , elapsedTime , clicksUser , clicksAgent 
								, sum(offeredChipSetUserCopy),sum(offeredChipSetAgentCopy)])
							agentAccepted=3
							if roundNumber%2==0:
								userTurn=2
								NextRound = 1
								if roundNumber%10==6:
									userTurn=1
									
							else:
								userTurn=1
							break
				elif (pos[0]>800 and pos[0]<900):
					if pos[1] >500 and pos[1] < 540:
						postResponsePointsUser = PostPointUser
						postResponsePointsAgent = PostPointAgent
						postResponseStatusUser = PostOfferStatusUser
						postResponseStatusAgent = PostOfferStatusAgent

						if PostPointUser>BeginningPointUser:

							if PostPointUser >=PostPointAgent:
								hintText = "Nice thinking!"
							else:
								hintText = "Better luck next round"
						elif PostPointUser<BeginningPointUser:
							hintText = "Maybe you should've rejected the offer"


						userPosX = copy.copy(acceptXUser)
						userPosY = copy.copy(acceptYUser)
						agentPosX = copy.copy(acceptXAgent)
						agentPosY = copy.copy(acceptYAgent)
						elapsedTime = time.time()- startTime
						userAccepted=1
						agentAccepted=3
					NextRound = 1	

				elif pos[0]>925 and pos[0]<1025:
					if pos[1] >500 and pos[1] < 540: 

						postResponsePointsUser = BeginningPointUser
						postResponsePointsAgent = BeginningPointAgent
						postResponseStatusUser = BeginningUser
						postResponseStatusAgent = BeginningAgent

						if PostPointUser<BeginningPointUser:
							if BeginningPointUser >BeginningPointAgent:
								hintText = "Nice thinking"
							elif BeginningPointUser<BeginningPointAgent:
								hintText = "You didnt think this through"
						elif PostPointUser>BeginningPointUser:
							hintText = "Maybe you should've accepted the offer"


						userPosX = copy.copy(rejectXUser)
						userPosY = copy.copy(rejectYUser)
						agentPosX = copy.copy(rejectXAgent)
						agentPosY = copy.copy(rejectYAgent)
						elapsedTime = time.time()- startTime
						userAccepted=0
						agentAccepted=3			
					NextRound = 1

						# pprint(offeredChipSetAgent)
		if userTurn != 3:
			break
		playSurface.fill(white)
		drawGrid()
		drawChips()
		showChipSet()
		PersistanceFlag=0
		drawChipsAgent()
		drawOfferedChipsAgent()
		drawOfferedChipsUser()
		showChipSetAgent()
		showButtonsChips()
		showOfferedChipSetAgent()
		showOfferedChipSetUser()
		showResponse()
		if agentAccepted!=4:
			showPoints()
			drawButtons()
		pygame.display.flip()
		fpsController.tick(50)

	route =[]
	offeredChipSetUser = [0,0,0,0,0]
	offeredChipSetAgent = [0,0,0,0,0]
	remainingChipSetUser = ChipSetUserBeginning
	remainingChipSetAgent = ChipSetAgentBeginning
	userPosX = copy.copy(userPosXCSV)
	userPosY = copy.copy(userPosYCSV)
	agentPosX = copy.copy(agentPosXCSV)
	agentPosY = copy.copy(agentPosYCSV)
	hintText = ""
	userAccepted=3
	clicksAgent = 0
	clicksUser = 0
	requiredPathUser = [[0,0,0,0,0], #DownDownRightRight
	[0,0,0,0,0], #RightRightDownDown
	[0,0,0,0,0], #DownRightDownRight
	[0,0,0,0,0]] #RightDownRightDown

	requiredPathAgent = [[0,0,0,0,0], #DownDownRightRight
	[0,0,0,0,0], #RightRightDownDown
	[0,0,0,0,0], #DownRightDownRight
	[0,0,0,0,0]] #RightDownRightDown
file.writerow(["//////////////////////"])
# testFile = csv.writer(open('test2.csv','a'))
# with open('test.csv', 'rb') as csvfile:
# 	spamreader = csv.reader(csvfile, delimiter='\n', quotechar='|')
# 	for row in spamreader:
# 		testFile.writerow(row)




